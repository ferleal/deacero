var elixir = require('laravel-elixir');

/*
|--------------------------------------------------------------------------
| Elixir Asset Management
|--------------------------------------------------------------------------
|
| Elixir provides a clean, fluent API for defining some basic Gulp tasks
| for your Laravel application. By default, we are compiling the Sass
| file for our application, as well as publishing vendor resources.
|
*/

elixir(function(mix) {
    mix.less('app.less');
    mix.scripts([
        '../bower/jquery/dist/jquery.min.js',
        '../bower/bootstrap/dist/js/bootstrap.js',
        //'../bower/angular/angular.js',
        '../bower/moment/moment.js',
        '../bower/moment/locale/es.js',
        //'../bower/angular-ui-bootstrap-bower/ui-bootstrap-tpls.js',
        //'../bower/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.js',
        //'../bower/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
        'deacero.js'
    ], 'public/js/vendor.js');

    mix.scripts([
        '../bower/html5shiv/dist/html5shiv.js',
        '../bower/respond/dest/respond.src.js',
    ],'public/js/onlyie.js')


    mix.styles([
        //'../bower/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.css',
        'deacero.css'
    ]);
    mix.copy('./resources/assets/bower/font-awesome/fonts','public/fonts/');
    mix.copy('./resources/assets/bower/bootstrap/fonts','public/fonts/');
    mix.copy('./resources/assets/img/','public/images/');
    mix.copy('./resources/assets/download/','public/files/');
    mix.copy('./resources/assets/font/','public/css/fonts/');
});

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('home');
});

Route::get('/product/gallery/{id}', 'ProductsGalleriesController@show');
Route::get('/product/doc/{id}', 'ProductsDocumentationsController@show');
Route::get('/product/specification/{id}', 'ProductsSpecificationsController@show');
Route::get('/product/{id}', 'ProductsController@show');

Route::get('/quotation', function () {
    return view('quotation');
});
Route::get('/product', function () {
    return view('product');
});
Route::get('/leed', function () {
    return view('leed');
});
Route::get('/almasa', function () {
    return view('almasa');
});
Route::get('/warranty', function () {
    return view('warranty');
});

Route::get('/contact','ContactsController@index');

Route::get('/event','EventsController@index');

/**
 * Api
 */

Route::get('api/contact','ContactsController@index');


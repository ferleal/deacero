<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductsSpecificationsController extends Controller
{

    public function show($id)
    {
        $product = Product::where(['routeName' => $id])->first();
        if ($product !== null) {
            return view('productSpecification'.ucfirst($product->routeName), compact('id', 'product'));
        }
        else{
            #here we make redirect, because product is incorrect
            return Redirect::to('product');
        }
    }


}

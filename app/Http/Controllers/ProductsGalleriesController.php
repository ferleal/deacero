<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductsGallery;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class ProductsGalleriesController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $product = Product::where(['routeName' => $id])->first();
        if ($product !== null) {
            $productGallery = ProductsGallery::where(['product_id' => $product->id])->get()->toArray();
            return view('productGallery', compact('id', 'product', 'productGallery'));

        }
        else{
            #here we make redirect, because product is incorrect
            return Redirect::to('product');
        }
    }


}

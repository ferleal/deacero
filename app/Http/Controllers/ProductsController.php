<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class ProductsController extends Controller
{


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $product = Product::where(['routeName' => $id])->first();
         if ($product !== null){
            !empty($product->features) ? $product->features = json_decode($product->features, true) : null;
            !empty($product->utilization) ? $product->utilization = json_decode($product->utilization, true) : null;
            !empty($product->icon) ? $product->icon = json_decode($product->icon, true) : null;
            return view('productInfo', compact('id', 'product'));
        }
        else{
            #here we make redirect, because product is incorrect
            return Redirect::to('product');
        }

    }
}

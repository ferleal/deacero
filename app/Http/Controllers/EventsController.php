<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Requests;
use Carbon\Carbon;

class EventsController extends Controller
{

    public function index()
    {
        $events       = Event::orderBy('eventDate', 'ASC')->get([ 'name', 'location', 'eventDate' ]);
        $today        = Carbon::today();
        $pastEvents   = [ ];
        $futureEvents = [ ];
        foreach ($events as $event) {

            $eventDate = new Carbon($event->eventDate);
            if ($today->gte($eventDate)) {
                $pastEvents[$eventDate->year][] = [ 'name' => $event->name, 'location' => $event->location ];
            }

            if ($today->lte($eventDate)) {
                $futureEvents[$eventDate->year][] = [ 'name' => $event->name, 'location' => $event->location ];
            }
        }

        return view('event', [ 'pastEvents' => $pastEvents, 'futureEvents' => $futureEvents ]);
    }
}

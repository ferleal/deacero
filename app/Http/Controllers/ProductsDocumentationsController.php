<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductsDocumentation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductsDocumentationsController extends Controller
{

    public function show($id)
    {
        $product = Product::where(['routeName' => $id])->first();
        if ($product !== null) {
            $productDocumentation = ProductsDocumentation::where(['product_id' => $product->id])->get()->toArray();
            return view('productDocumentation', compact('id', 'product', 'productDocumentation'));
        }
        else{
            #here we make redirect, because product is incorrect
            return Redirect::to('product');
        }
    }


}

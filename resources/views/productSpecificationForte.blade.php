@extends('app')

@section('content')
    @include('productMenu')
    <div class="container-fluid prodDescBox">
        <div class="row">
            <div class="col-xs-12 col-md-9">
                <div class="row rowSpacer">
                    <div class="col-md-7 proDescHeader">
                        <span>ESPECIFICACIONES</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 proDescHeaderDivider"></div>
                </div>
                <div class="row" style="margin-left: 0px;">
                    <div class="row" style="margin-top: 40px; margin-bottom: 20px;">
                        <div class="col-xs-12 col-md-11 proDescHeaderDivider"></div>
                    </div>
                    <div class="row">
                        <table class="col-xs-12 col-md-11 tbl-black-orange">
                            <thead>
                                <tr>
                                    <th>Dise&ntilde;os</th>
                                    <th>Altura</th>
                                    <th>Ancho</th>
                                    <th colspan="2">Calibre</th>
                                    <th>Altura poste</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td rowspan="4" class="black border-white">Plegada</td>
                                    <td>m</td>
                                    <td>m</td>
                                    <td>cal.</td>
                                    <td>mm</td>
                                    <td>m</td>
                                </tr>
                                <tr>
                                    <td>1.00</td>
                                    <td>2.50</td>
                                    <td>8</td>
                                    <td>4</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <td>1.93</td>
                                    <td>2.50</td>
                                    <td>8</td>
                                    <td>4</td>
                                    <td>2.70</td>
                                </tr>
                                <tr>
                                    <td>2.44</td>
                                    <td>2.50</td>
                                    <td>8</td>
                                    <td>4</td>
                                    <td>3.20</td>
                                </tr>
                                <tr>
                                    <td rowspan="4" class="black">Plana</td>
                                    <td>2.00</td>
                                    <td>2.50</td>
                                    <td>8</td>
                                    <td>4</td>
                                    <td>2.70</td>
                                </tr>
                                <tr>
                                    <td>2.50</td>
                                    <td>2.50</td>
                                    <td>8</td>
                                    <td>4</td>
                                    <td>3.20</td>
                                </tr>
                                <tr>
                                    <td>3.00</td>
                                    <td>2.50</td>
                                    <td>8</td>
                                    <td>4</td>
                                    <td>4.32</td>
                                </tr>
                                <tr>
                                    <td>3.60</td>
                                    <td>2.50</td>
                                    <td>8</td>
                                    <td>4</td>
                                    <td>4.92</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row" style="margin-top: 40px; margin-bottom: 20px;">
                        <div class="col-xs-12 col-md-11 proDescHeaderDivider"></div>
                    </div>
                    <div class="row">
                        <table class="col-xs-12 col-md-11 tbl-black-orange">
                            <thead>
                                <tr class="fat">
                                    <th>Resistencia a la tensión del alambre</th>
                                    <th>Capa de zinc</th>
                                    <th colspan="2">Abertura</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="fat">
                                    <td>Lb/ln2</td>
                                    <td>grs/m2</td>
                                    <td>plg</td>
                                    <td>cm</td>
                                </tr>
                                <tr>
                                    <td>70,000 - 90,000</td>
                                    <td>55</td>
                                    <td>1/2” x 3”</td>
                                    <td>7.62 x 1.27</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-3 rowSpacer">
                <img alt="" src="{{asset('images/products/fortespec1.jpg')}}" class="img-responsive" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-9">
                <div class="row rowSpacer">
                    <div class="col-md-7 proDescHeader">
                        <span>ACCESORIOS DE INSTALACI&Oacute;N</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 proDescHeaderDivider"></div>
                </div>
                <div class="row" style="margin-top: 100px;">
                    <ul class="clearfix ul-orange">
                        <li class="col-xs-12 col-md-3">
                           Poste
                           <div>Con remache roscado</div>
                           <img alt="" src="{{asset('images/products/poste3.jpg')}}" class="img-responsive" />
                        </li>
                        <li class="col-xs-12 col-md-3">
                            Tapa
                            <div>Polipropileno</div>
                            <img alt="" src="{{asset('images/products/tapa4.jpg')}}" class="img-responsive" />
                        </li>
                        <li class="col-xs-12 col-md-3">
                            Tornillos
                            <div>De seguridad Torx</div>
                            <img alt="" src="{{asset('images/products/tornillos2.jpg')}}" class="img-responsive" />
                        </li>
                       <li class="col-xs-12 col-md-3">
                            Grapa met&aacute;lica
                            <div>Galvanizada</div>
                            <img alt="" src="{{asset('images/products/grapametalica.jpg')}}" class="img-responsive" />
                        </li>
                    </ul>
                </div>
                <div class="row" style="margin-top: 80px; margin-bottom: 50px;">
                    <div class="col-xs-12 col-md-11 proDescHeaderDivider"></div>
                </div>
                <div class="row">
                    <ul class="ul-colors list-unstyled col-xs-12 col-md-12">
                        <li class="col-xs-12 col-md-1" style=" padding-top: 10px;">Colores</li>
                        <li class="col-xs-12 col-md-10">
                            <img alt="" src="{{asset('images/products/verde.jpg')}}" class="img-responsive" align="left" style="padding-left: 0px;" />
                            <img alt="" src="{{asset('images/products/negro.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/cafe.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/blanco.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/azul.jpg')}}" class="img-responsive" align="left"  style="padding-left: 10px;" />
                            <img alt="" src="{{asset('images/products/amarillo.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/rojo.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/gris.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                        </li>
                    </ul>
                </div>
                <div class="row" style="margin-top: 50px; margin-bottom: 50px;">
                    <div class="col-xs-12 col-md-11 proDescHeaderDivider"></div>
                </div>
            </div>
        </div>
    </div>




@endsection
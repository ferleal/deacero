@extends('app')

@section('content')
    @include('productMenu')
    <div  class="container-fluid prodDescBox"  >
        <div class="row rowSpacer">
            <div class="col-md-4 proDescHeader">
                <span>DOCUMENTACI&Oacute;N</span>
            </div>
        </div>
        <div class="row" >
            <div class="col-md-4 proDescHeaderDivider"></div>
        </div>
        <div class="row">
            <div class="col-md-12 proDescTxt table-responsive">
                <table class="table proDocTbl">
                    <thead>
                        <tr>
                            <th>PANEL</th>
                            <th>Archivo DWG</th>
                            <th>Archivo PDF</th>
                            <th>Especificaciones</th>
                            <th>Descripci&oacute;n T&eacute;cnica</th>
                            <th>Tabla de Calidad</th>
                            <th>Folleto</th>
                            <th>Manual de Instalaci&oacute;n</th>
                            <th>Gu&iacute;a de Mantenimiento</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($productDocumentation as $index => $doc)
                        <tr>
                            <td class="{{ ($index % 2) ? "blackgrey" : "grey" }}">{{$doc['panel']}}</td>
                            <td class="{{ ($index % 2) ? "blackgrey" : "grey" }}"><a href="{{$doc['file_dwg']}}"><img alt="" src="{{asset('images/dwg-24.png')}}" class="img-responsive" /></a> </td>
                            <td class="{{ ($index % 2) ? "blackgrey" : "grey" }}"><a href="{{$doc['file_pdf']}}"><img alt="" src="{{asset('images/pdf.png')}}" class="img-responsive" /></a> </td>
                            @if ($doc['order'] == 1)
                             <td rowspan="5" class="grey" ><a href="{{$product['specifications_file']}}"><img alt="" src="{{asset('images/pdf.png')}}" class="img-responsive" /></a> </td>
                             <td rowspan="5" class="grey" ><a href="{{$product['description_file']}}"><img alt="" src="{{asset('images/pdf.png')}}" class="img-responsive" /></a> </td>
                             <td rowspan="5" class="grey" ><a href="{{$product['quality_file']}}"><img alt="" src="{{asset('images/pdf.png')}}" class="img-responsive" /></a> </td>
                             <td rowspan="5" class="grey" ><a href="{{$product['brochure_file']}}"><img alt="" src="{{asset('images/pdf.png')}}" class="img-responsive" /></a> </td>
                             <td rowspan="5" class="grey" ><a href="{{$product['manual_file']}}"><img alt="" src="{{asset('images/pdf.png')}}" class="img-responsive" /></a> </td>
                             <td rowspan="5" class="grey" ><a href="{{$product['maintenance_file']}}"><img alt="" src="{{asset('images/pdf.png')}}" class="img-responsive" /></a> </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row rowSpacer">
                <div class="col-md-12 col-md-offset-4" style="padding-top: 50px;">
                    <iframe width="420" height="315" src="{{$product['video']}}"></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection
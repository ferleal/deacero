@extends('app')

@section('content')
    @include('productMenu')
    <div  class="container-fluid prodDescBox"  >
        <div class="row rowSpacer">
            <div class="col-md-11 proDescHeader">
                <span>GALERÍA DE IMÁGENES</span>
            </div>
        </div>
        <div class="row" >
            <div class="col-md-11  proDescHeaderDivider"></div>
        </div>
        <div class="row">
            <div class="col-md-12 proDescTxt">
                @foreach ($productGallery as $index =>$images)
                @if ( ($index % 3)  == 0 )
                <div class="row">
                @endif
                    <div class="col-md-4 col-xs-4 " style="padding-bottom: 30px;">
                        <img alt="" src="{{asset('images/products/'.$images['picture'])}}" class="img-responsive" />

                    </div>
                @if ( ($index % 3)  == 2 )
                </div>
                @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection
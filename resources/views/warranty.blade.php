@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-12 textHeader">
            GARANT&Iacute;A
        </div>
    </div>
    <div  class="container-fluid prodDescBox" style="padding-top: 40px;"  >
        <div class="row">
            <div class="col-md-5 proDescHeader">
                <span>GARANTÍA DE FABRICACIÓN</span>
            </div>
        </div>
        <div class="row" >
            <div class="col-md-5  proDescHeaderDivider"></div>
        </div>
        <div class="row">
            <div class="col-md-5 proDescTxt">
                La Reja Deacero Galvanizada y con Poliéster Termo - Endurecido ofrece una
                garantía de 10 años contra defectos de fabricación, corrosión y desgaste de
                su capa protectora, en sus paneles, postes y abrazaderas.
                <br /><br /><br />
                Descargue la garantía en formato .PDF para mas detalles.
                <br /><br /><br />
                Es indispensable cumplir con la Guía de Mantenimiento para hacer válida la
                garantía. Puede descargarla en la sección de documentos en cada producto
            </div>
        </div>
    </div>
@endsection
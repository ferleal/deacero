@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-12 textHeader">
            ALMASA
        </div>
    </div>
    <div class="row">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center" style="padding-top: 40px;">
                    <img src="images/almasabig.png" width="233px" />
                </div>
            </div>
            <br/><br/>
            <div class="row" style="padding-bottom: 20px;">
                <div class="col-md-6">
                    Alambres y Mallas S.A., es una organización que cuenta con más de 40 años
                    de experiencia en el suministro de productos de acero, para dar solución a
                    proyectos arquitectónicos, a la industria y al agro, siempre teniendo presente
                    las necesidades de nuestros clientes bajo el más estricto cumplimiento de
                    estándares de calidad, posicionándonos en el mercado como una de las
                    mejores marcas a nivel nacional en alambres trefilados, productos laminados
                    y figurados.<br /> <br />
                    Alambres y Mallas S.A. está integrada por cuatro centros de producción
                    situados en Bogotá, Soacha, Malambo, Candelaria y Girardota,
                    que permiten una gran cobertura en el país. La Planta de Malambo tiene una
                    UBICACIÓN ESTRATÉGICA dentro de la posición geográfica nacional y del
                    hemisferio americano, que da la cercanía a un puerto, facilitando el proceso
                    de comercio exterior.
                </div>
                <div class="col-md-6">
                    Nuestro talento humano fundamentado en valores corporativos como el
                    RESPETO, EQUIDAD, HONESTIDAD Y COMPROMISO, respalda nuestra
                    propuesta de valor mediante la interpretación de las necesidades del
                    mercado y posteriormente la adaptación de productos y servicios como
                    alternativas de solución.
                    <br/> <br/>
                    Nuestra cultura organizacional orientada a los resultados, a la satisfacción
                    del cliente y a la ética en los negocios, constituye un marco de referencia
                    en el desempeño de nuestras actividades buscando consolidar la
                    excelencia en el servicio al Cliente.
                </div>
            </div>
        </div>
    </div>
@endsection
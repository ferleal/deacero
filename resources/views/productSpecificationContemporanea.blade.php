@extends('app')

@section('content')
    @include('productMenu')

    <div class="container-fluid prodDescBox">
        <div class="row">
            <div class="col-xs-12 col-md-9">
                <div class="row rowSpacer">
                    <div class="col-md-7 proDescHeader">
                        <span>ESPECIFICACIONES</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 proDescHeaderDivider"></div>
                </div>
                <div class="row" style="margin-left: 0px; margin-top: 20px;">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="row" style="margin-top: 0px; margin-bottom: 20px;">
                                <h4 class="tbl-header-specification">Panel</h4>
                                <div class="col-xs-12 col-md-9 proDescHeaderDivider"></div>
                            </div>
                            <div class="row">
                                <table class="col-xs-12 col-md-9 tbl-black-orange">
                                <thead>
                                    <tr class="fat">
                                        <th>Diseños</th>
                                        <th>Altura</th>
                                        <th>Ancho</th>
                                        <th>Abertura</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td rowspan="7" class="black">868 y 656</td>
                                        <td>m</td>
                                        <td>m</td>
                                        <td>mm</td>
                                    </tr>
                                    <tr>
                                        <td>1.00</td>
                                        <td>2.50</td>
                                        <td>50 x 200</td>
                                    </tr>
                                    <tr>
                                        <td>1.20</td>
                                        <td>2.50</td>
                                        <td>50 x 200</td>
                                    </tr>
                                    <tr>
                                        <td>1.60</td>
                                        <td>2.50</td>
                                        <td>50 x 200</td>
                                    </tr>
                                    <tr>
                                        <td>1.80</td>
                                        <td>2.50</td>
                                        <td>50 x 200</td>
                                    </tr>
                                    <tr>
                                        <td>2.00</td>
                                        <td>2.50</td>
                                        <td>50 x 200</td>
                                    </tr>
                                    <tr>
                                        <td>2.40</td>
                                        <td>2.50</td>
                                        <td>50 x 200</td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="row" style="margin-top: 0px; margin-bottom: 20px;">
                                <h4 class="tbl-header-specification">Poste</h4>
                                <div class="col-xs-12 col-md-10 proDescHeaderDivider"></div>
                            </div>
                            <div class="row">
                                <table  class="col-xs-12 col-md-10 tbl-black-orange">
                                <thead>
                                    <tr>
                                        <th>Altura reja</th>
                                        <th>Cimentaci&oacute;n</th>
                                        <th>Altura poste</th>
                                        <th>Separador, remache y tornillo de seguridad</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>m</td>
                                        <td>m</td>
                                        <td>m</td>
                                        <td>#</td>
                                    </tr>
                                    <tr>
                                        <td>1.00</td>
                                        <td>0.5</td>
                                        <td>1.58</td>
                                        <td>4</td>
                                    </tr>
                                    <tr>
                                        <td>1.20</td>
                                        <td>0.5</td>
                                        <td>1.78</td>
                                        <td>4</td>
                                    </tr>
                                    <tr>
                                        <td>1.60</td>
                                        <td>0.5</td>
                                        <td>2.18</td>
                                        <td>5</td>
                                    </tr>
                                    <tr>
                                        <td>1.80</td>
                                        <td>0.6</td>
                                        <td>2.48</td>
                                        <td>6</td>
                                    </tr>
                                    <tr>
                                        <td>2.00</td>
                                        <td>0.6</td>
                                        <td>2.68</td>
                                        <td>6</td>
                                    </tr>
                                    <tr>
                                        <td>2.40</td>
                                        <td>0.6</td>
                                        <td>3.08</td>
                                        <td>7</td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>




                    <div class="row" style="margin-top: 60px;">
                        <div class="col-xs-12 col-md-6">
                            <div class="row" style="margin-top: 0px; margin-bottom: 20px;">
                                <h4 class="tbl-header-specification">Varilla Vertical</h4>
                                <div class="col-xs-12 col-md-9 proDescHeaderDivider"></div>
                            </div>
                            <div class="row">
                                <table class="col-xs-12 col-md-9 tbl-black-orange">
                                    <thead>
                                        <tr>
                                            <th>Diseños</th>
                                            <th>Resistencia a la tensi&oacute;n</th>
                                            <th colspan="2">Calibre</th>
                                            <th>Capa de zinc</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>Lb/plg2</td>
                                            <td>cal.</td>
                                            <td>mm</td>
                                            <td>grs/m2</td>
                                        </tr>
                                        <tr>
                                            <td>868</td>
                                            <td>75,000 - 105,000</td>
                                            <td>4</td>
                                            <td>6</td>
                                            <td>92</td>
                                        </tr>
                                        <tr>
                                            <td>656</td>
                                            <td>75,000 - 105,000</td>
                                            <td>6</td>
                                            <td>5</td>
                                            <td>92</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="row" style="margin-top: 0px; margin-bottom: 20px;">
                                <h4 class="tbl-header-specification">Doble Varilla Horizontal</h4>
                                <div class="col-xs-12 col-md-10 proDescHeaderDivider"></div>
                            </div>
                            <div class="row">
                                <table  class="col-xs-12 col-md-10 tbl-black-orange">
                                    <thead>
                                        <tr>
                                            <th>Diseños</th>
                                            <th>Resistencia <br/>a la tensi&oacute;n</th>
                                            <th colspan="2">Calibre</th>
                                            <th>Capa de zinc</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>Lb/plg2</td>
                                            <td>cal.</td>
                                            <td>mm</td>
                                            <td>grs/m2</td>
                                        </tr>
                                        <tr>
                                            <td>868</td>
                                            <td>80,000 - 95,000</td>
                                            <td>1/0</td>
                                            <td>8</td>
                                            <td>92</td>
                                        </tr>
                                        <tr>
                                            <td>656</td>
                                            <td>75,000 - 105,000</td>
                                            <td>4</td>
                                            <td>6</td>
                                            <td>92</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-xs-12 col-md-3 rowSpacer">
                <img alt="" src="{{asset('images/products/contemporaneaspec1.jpg')}}" class="img-responsive" />
            </div>
        </div>
        <div class="row" style="margin-top: 80px;">
            <div class="col-xs-12 col-md-9">
                <div class="row rowSpacer">
                    <div class="col-md-7 proDescHeader">
                        <span>ACCESORIOS DE INSTALACI&Oacute;N</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 proDescHeaderDivider"></div>
                </div>
                <div class="row" style="margin-top: 100px;">
                    <ul class="clearfix ul-orange">
                        <li class="col-xs-12 col-md-2">
                           Separador
                           <div>Polipropileno</div>
                           <img alt="" src="{{asset('images/products/separador.jpg')}}" class="img-responsive" />
                        </li>
                        <li class="col-xs-12 col-md-2">
                            Tapa
                            <div>Polipropileno</div>
                            <img alt="" src="{{asset('images/products/tapa3.jpg')}}" class="img-responsive" />
                        </li>
                        <li class="col-xs-12 col-md-2">
                            Tornillos
                            <div>Torx de seguridad</div>
                            <img alt="" src="{{asset('images/products/tornillos.jpg')}}" class="img-responsive" />
                        </li>
                       <li class="col-xs-12 col-md-2 md-20percent-specification-acces">
                            Barra de Sujeci&oacute;n
                            <div>Solera 3/16” x 1 1/2”</div>
                            <img alt="" src="{{asset('images/products/barrasujecion.jpg')}}" class="img-responsive" />
                        </li>
                       <li class="col-xs-12 col-md-3">
                            Poste - <span class="text-normal-specifications">Calibre 14</span>
                            <div>Dimensión 2 1/2” x 1 1/2</div>
                            <img alt="" src="{{asset('images/products/poste2.jpg')}}" class="img-responsive" />
                        </li>
                    </ul>
                </div>
                <div class="row" style="margin-top: 80px; margin-bottom: 50px;">
                    <div class="col-xs-12 col-md-11 proDescHeaderDivider"></div>
                </div>
                <div class="row">
                    <ul class="ul-colors list-unstyled col-xs-12 col-md-12">
                        <li class="col-xs-12 col-md-1" style=" padding-top: 10px;">Colores</li>
                        <li class="col-xs-12 col-md-10">
                            <img alt="" src="{{asset('images/products/verde.jpg')}}" class="img-responsive" align="left" style="padding-left: 0px;" />
                            <img alt="" src="{{asset('images/products/negro.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/cafe.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/blanco.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/azul.jpg')}}" class="img-responsive" align="left"  style="padding-left: 10px;" />
                            <img alt="" src="{{asset('images/products/amarillo.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/rojo.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/gris.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                        </li>
                    </ul>
                </div>
                <div class="row" style="margin-top: 50px; margin-bottom: 50px;">
                    <div class="col-xs-12 col-md-11 proDescHeaderDivider"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
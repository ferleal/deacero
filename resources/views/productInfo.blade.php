@extends('app')

@section('content')
    @include('productMenu')

    <div class="row rowSpacer" >
        <div class="col-md-12">
            <img alt="{{$product->name}}" width="100%" style="height: 500px;"  class="img-responsive" src="{{asset('images/products/'.$product->image)}}" />
        </div>
    </div>

    <div class="container-fluid">
        <div class="row rowSpacer">
            <div class="col-md-6" style="background: #8B8B8B; padding: 10px; font-weight:bold; ">
                    <span>{{$product->description}}</span>
            </div>
            <div class="col-md-6 text-right">
                @foreach ($product->icon as $icon)
                    <img alt="" src="{{asset('images/products/'.$icon)}}" />
                @endforeach
            </div>
        </div>
        <div class="row rowSpacer">
            <div class="col-md-6  " style=" margin: 0px; padding: 0px;  ">
                <div  class="container-fluid prodDescBox"  >
                    <div class="row">
                        <div class="col-md-11 proDescHeader">
                            <span>CARACTER&Iacute;STICAS</span>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-md-11  proDescHeaderDivider"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 proDescTxt">
                            <ul>
                                @foreach ($product->features as $features)
                                    <li>
                                        <h5>{{$features['name']}}</h5>
                                        <p>{{$features['description']}}</p>
                                    </li>
                                @endforeach
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6  " style=" margin: 0px; padding: 0px;  ">
                <div  class="container-fluid prodDescBox"  >
                    <div class="row">
                        <div class="col-md-11 proDescHeader">
                            <span>APLICACIONES</span>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-md-11  proDescHeaderDivider"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 proDescTxt">
                            <ul>
                                @foreach ($product->utilization as $utilization)
                                    <li>
                                        {{$utilization}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
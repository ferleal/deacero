@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-12 textHeader">
            CONTACTO
        </div>
    </div>
    <div class="container">
        @foreach($contacts as $contact)
        <div class="row">
            <div class="col-md-6  " style=" margin-top: 40px; padding: 0px;  ">
                <div  class="container-fluid prodDescBox"  >
                    <div class="row">
                        <div class="col-md-11 proDescHeader">
                            <span>{{$contact->zone}}</span>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-md-11  proDescHeaderDivider"></div>
                    </div>
                    <div class="row" style="padding-bottom: 20px;">
                        <div class="col-md-11 proDescTxt">
                            <span style="font-weight: bold; display: block;">{{ $contact->name }}</span>
                            <span style="display: block;"><i class="fa fa-fw"></i> {{ $contact->tel }}</span>
                            <span style="display: block;"><i class="fa fa-fw"></i> {{ $contact->email }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection
<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0px;">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar2" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbar2" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li > <a href="/product/{{$id}}">DESCRIPCIÓN</a></li>
                <li class="headerDivider2"></li>
                <li><a href="/product/specification/{{$id}}">ESPECIFICACIONES</a></li>
                <li class="headerDivider2"></li>
                <li><a href="/product/gallery/{{$id}}">GALERÍA DE IMÁGENES</a></li>
                <li class="headerDivider2"></li>
                <li><a href="/product/doc/{{$id}}">DOCUMENTACIÓN</a></li>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<div class="row">
    <div class="col-md-12 textHeader">
        PRODUCTOS / <span style="color:#FF5325">{{$product->name}}</span>
    </div>
</div>
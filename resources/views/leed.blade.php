@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-xs-12 textHeader">LEED Y SUSTENTABILIDAD</div>
    </div>
    <div class="container-fluid" style="margin-bottom: 20px;" >
        <div class="row rowSpacer">
            <div class="col-md-12"  >
                <div class="row">
                    <div class="col-md-6  " style=" margin: 0px; padding: 0px;  ">
                        <div  class="container-fluid prodDescBox"  >
                            <div class="row">
                                <div class="col-md-11 proDescHeader" style="font-size: 14px;">
                                    <span>INICIATIVA SUSTENTABLE</span>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-md-11  proDescHeaderDivider"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-11 proDescTxt" style="font-size: 14px;">
                                    Grupo Deacero y todas las marcas de sus líneas de productos visualizan a la
                                    sustentabilidad como el eje rector de sus operaciones, generando valor en lo
                                    social, económico y ambiental.<br /> <br />
                                    Deacero esta acreditado por la World Steel Association como miembro activo
                                    de su programa Climate Action, gracias a su estricto control de consumo de
                                    energía y agua en la fabricación de sus productos, así como en la emisión de
                                    partículas en el medio ambiente.<br /> <br />
                                    Reja Deacero es un producto fabricado con un acabado libre de componentes
                                    orgánicos volátiles (VOC).
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6   " style="  margin: 0px; padding: 0px; ">
                        <div  class="container-fluid prodDescBox"  >
                            <div class="row">
                                <div class="col-md-5 col-xs-6">
                                    <img src="{{ asset('images/leed1.jpg') }}" class="img-responsive">
                                </div>
                                <div class="col-md-5 col-xs-6">
                                    <img src="{{ asset('images/leed2.jpg') }}" class="img-responsive">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row rowSpacer">
                    <div class="col-md-12 col-xs-12">
                        <img src="{{ asset('images/leedBar.jpg') }}" width="100%"  class="img-responsive">
                    </div>
                </div>
                <div class="row rowSpacer">
                    <div class="col-md-5 col-xs-7" style="padding-left: 0px; padding-bottom: 20px;">
                        <img src="{{ asset('images/leed3.jpg') }}"  class="img-responsive" >
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <div  class="container-fluid prodDescBox"  >
                            <div class="row">
                                <div class="col-md-12 proDescHeader" style="font-size: 14px;">
                                    <span>CERTIFICACIÓN LEED Y REJA DEACERO</span>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-md-12  proDescHeaderDivider"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" style="font-size: 14px; padding-left: 0px;">
                                    <br /><br />
                                    Deacero es miembro activo del United States Green Building Council, entidad rectora del
                                    programa de certificación LEED para proyectos de construcción sustentables.
                                    La certificación LEED promueve el uso de productos renovables o fabricados con insumos
                                    reciclados en diversos proyectos de construcción.<br /><br />
                                    Reja Deacero, como sistema integral de reja, aplica a ciertos créditos necesarios para lograr
                                    la certificación LEED de aquellas obras y empresas con ésta meta en mente.
                                    En específico Reja Deacero aplica a los siguientes apartados LEED: MR 4.1 y 4.2 – Créditos
                                    relacionados al uso de productos hechos con materiales reciclados y con la capacidad de ser
                                    reciclables.<br /><br />
                                    De esta manera Reja Deacero tiene un potencial de aplicación hasta a cuatro
                                    créditos LEED, en la búsqueda de la certificación de su proyecto.<br /><br />
                                </div>
                            </div>
                            <div class="row rowSpacer" >
                                <img src="{{ asset('images/leed4.jpg') }}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
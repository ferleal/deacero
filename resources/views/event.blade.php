@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-12 textHeader">
            EVENTOS
        </div>
    </div>
    <div class="container">
        <div class="row rowSpacer" style="margin-bottom: 40px;">
            <div class="col-md-12"  >
                <div class="row"   >
                    <div class="col-md-6  " >
                        <div  class="container-fluid prodDescBox"  >
                            <div class="row">
                                <div class="col-md-11 proDescHeader">
                                    <span>EVENTOS RECIENTES</span>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-md-11  proDescHeaderDivider"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-11 proDescTxt">
                                    @foreach($pastEvents as $pkey => $pevent)
                                        <span class="eventYear">{{ $pkey }}</span>
                                        @foreach($pevent as $pe)
                                        <span class="eventName">{{ $pe['name'] }}</span>
                                        <span class="eventLocation">{{ $pe['location'] }}</span>
                                        @endforeach
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6  " >
                        <div  class="container-fluid prodDescBox"  >
                            <div class="row">
                                <div class="col-md-11 proDescHeader">
                                    <span>PRÓXIMOS EVENTOS</span>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-md-11  proDescHeaderDivider"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-11 proDescTxt">
                                    @foreach($futureEvents as $fkey => $fevent)
                                        <span class="eventYear">{{ $fkey }}</span>
                                        @foreach($fevent as $fe)
                                            <span class="eventName">{{ $fe['name'] }}</span>
                                            <span class="eventLocation">{{ $fe['location'] }}</span>
                                        @endforeach
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="container">
                        <div class="col-md-12 ">
                            <div class="carousel slide" id="myCarousel">
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                                            <a href="#"><img src="{{ url('images/events/asocreto1.jpg') }}" class="img-responsive"></a></div>
                                    </div>
                                    <div class="item">
                                        <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                                            <a href="#"><img src="{{ url('images/events/asocreto2.jpg') }}" class="img-responsive"></a></div>
                                    </div>
                                    <div class="item">
                                        <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                                            <a href="#"><img src="{{ url('images/events/asocreto3.jpg') }}" class="img-responsive"></a></div>
                                    </div>
                                    <div class="item">
                                        <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                                            <a href="#"><img src="{{ url('images/events/charlaSca1.jpg') }}" class="img-responsive"></a></div>
                                    </div>
                                    <div class="item">
                                        <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                                            <a href="#"><img src="{{ url('images/events/charlaSca2.jpg') }}" class="img-responsive"></a></div>
                                    </div>
                                    <div class="item">
                                        <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                                            <a href="#"><img src="{{ url('images/events/expocamacol1.jpg') }}" class="img-responsive"></a></div>
                                    </div>
                                    <div class="item">
                                        <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                                            <a href="#"><img src="{{ url('images/events/expocamacol2.jpg') }}" class="img-responsive"></a></div>
                                    </div>
                                    <div class="item">
                                        <div class="col-lg-4 col-xs-4 col-md-4 col-sm-4">
                                            <a href="#"><img src="{{ url('images/events/expocamacol3.jpg') }}" class="img-responsive"></a></div>
                                    </div>
                                </div>
                                <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
    </div>
@endsection

@section('extraJS')
@{{
 $(document).ready(function() {
    $('#myCarousel').carousel({
        interval: 40000
    });
    $('.carousel .item').each(function(){
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));
        if (next.next().length>0) {
            next.next().children(':first-child').clone().appendTo($(this)).addClass('rightest');
        }
        else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
    });
 });
 }}
@endsection
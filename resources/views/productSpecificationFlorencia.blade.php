@extends('app')

@section('content')
    @include('productMenu')
    <div class="container-fluid prodDescBox">
        <div class="row">
            <div class="col-xs-12 col-md-9">
                <div class="row rowSpacer">
                    <div class="col-md-7 proDescHeader">
                        <span>ESPECIFICACIONES</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 proDescHeaderDivider"></div>
                </div>
                <div class="row" style="margin-left: 0px;">
                    <div class="row" style="margin-top: 40px; margin-bottom: 20px;">
                        <div class="col-xs-12 col-md-11 proDescHeaderDivider"></div>
                    </div>
                    <div class="row">
                        <table class="col-xs-12 col-md-11 tbl-black-orange">
                            <thead>
                                <tr>
                                    <th>Altura</th>
                                    <th>Longitud Panel</th>
                                    <th colspan="2">Calibre varilla formadora</th>
                                    <th colspan="2">Calibre varilla lineal</th>
                                    <th>Altura poste</th>
                                    <th>Profundidad cimentaci&oacute;n</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>m</td>
                                    <td>m</td>
                                    <td>cal.</td>
                                    <td>mm</td>
                                    <td>cal.</td>
                                    <td>mm</td>
                                    <td>m</td>
                                    <td>m</td>
                                </tr>
                                <tr>
                                    <td>0.80</td>
                                    <td>2.44</td>
                                    <td>4</td>
                                    <td>6</td>
                                    <td>1/0</td>
                                    <td>8</td>
                                    <td>1.4</td>
                                    <td>0.5</td>
                                </tr>
                                <tr>
                                    <td>1.20</td>
                                    <td>2.44</td>
                                    <td>4</td>
                                    <td>6</td>
                                    <td>1/0</td>
                                    <td>8</td>
                                    <td>1.8</td>
                                    <td>0.5</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row" style="margin-top: 40px; margin-bottom: 20px;">
                        <div class="col-xs-12 col-md-11 proDescHeaderDivider"></div>
                    </div>
                    <div class="row">
                        <table class="col-xs-12 col-md-11 tbl-black-orange">
                            <thead>
                                <tr>
                                    <th>Resistencia a la tensi&oacute;n del alambre</th>
                                    <th>Resistencia a la ruptura</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>m</td>
                                    <td>Lb/m&iacute;nimo</td>
                                </tr>
                                <tr>
                                    <td>55,000 - 70,000</td>
                                    <td>2,500</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-3 rowSpacer">
                <img alt="" src="{{asset('images/products/florenciaspec1.jpg')}}" class="img-responsive" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-9">
                <div class="row rowSpacer">
                    <div class="col-md-7 proDescHeader">
                        <span>ACCESORIOS DE INSTALACI&Oacute;N</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 proDescHeaderDivider"></div>
                </div>
                <div class="row" style="margin-top: 60px;">
                    <ul class="clearfix ul-orange">
                        <li class="col-xs-12 col-md-2">
                           Grapa oval
                           <div>55 x 34 x 10 mm</div>
                           <img alt="" src="{{asset('images/products/grapaoval.jpg')}}" class="img-responsive" />
                        </li>
                        <li class="col-xs-12 col-md-2">
                            Media grapa
                            <div>33 x 30 x 11 mm</div>
                            <img alt="" src="{{asset('images/products/mediagrapa.jpg')}}" class="img-responsive" />
                        </li>
                        <li class="col-xs-12 col-md-3">
                            Poste - <span class="text-normal-specifications">62 x 85 mm</span>
                            <div>Perfil de aluminio</div>
                            <img alt="" src="{{asset('images/products/poste.jpg')}}" class="img-responsive" />
                        </li>
                       <li class="col-xs-12 col-md-3">
                            <span class="text-normal-specifications">Pijas de acero inoxidable de</span>
                            <div>1/8” x 1” autotaladrante</div>
                        </li>
                    </ul>

                    <ul class="clearfix ul-orange">
                        <li class="col-xs-12 col-md-3">
                            Tapa - <span class="text-normal-specifications">- 70 x 90 x 30 mm</span>
                            <div>Polipropileno</div>
                            <img alt="" src="{{asset('images/products/tapa2.jpg')}}" class="img-responsive" />
                        </li>
                       <li class="col-xs-12 col-md-4">
                            Kit poste base <span class="text-normal-specifications">- 125 x 150 x 58 mm</span>
                            <div>Aluminio moldeado por inyección</div>
                            <img alt="" src="{{asset('images/products/kitposte.jpg')}}" class="img-responsive" />
                        </li>
                        <li class="col-xs-12 col-md-3" style="margin-top: 15px;">
                            <span class="text-normal-specifications">2 opresores de acero inoxidable</span>
                            <div class="text-normal-specifications">M 10 x 12 mm</div>
                            <ul class="ul-orange">

                                <li>
                                    <span class="text-normal-specifications">1 tornillo de acero inoxidable</span>
                                    <div class="text-normal-specifications">cabeza de coche M 10 x 60 mm</div>
                                    <div class="text-normal-specifications">con tuerca y arandela</div>
                                </li>
                                <li>
                                    <span class="text-normal-specifications">4 taquetes ancla 3/8” x 3”</span>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
                <div class="row" style="margin-top: 80px; margin-bottom: 50px;">
                    <div class="col-xs-12 col-md-11 proDescHeaderDivider"></div>
                </div>
                <div class="row">
                    <ul class="ul-colors list-unstyled col-xs-12 col-md-12">
                        <li class="col-xs-12 col-md-1" style=" padding-top: 10px;">Colores</li>
                        <li class="col-xs-12 col-md-10">
                            <img alt="" src="{{asset('images/products/verde.jpg')}}" class="img-responsive" align="left" style="padding-left: 0px;" />
                            <img alt="" src="{{asset('images/products/negro.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/cafe.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/blanco.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/azul.jpg')}}" class="img-responsive" align="left"  style="padding-left: 10px;" />
                            <img alt="" src="{{asset('images/products/amarillo.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/rojo.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/gris.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                        </li>
                    </ul>
                </div>
                <div class="row" style="margin-top: 50px; margin-bottom: 50px;">
                    <div class="col-xs-12 col-md-11 proDescHeaderDivider"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
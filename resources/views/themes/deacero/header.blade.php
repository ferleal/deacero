<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{{ getenv('PAGE_TITLE') }}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
        <!-- Theme style -->
        <link href="{{ asset('/css/all.css') }}" rel="stylesheet" type="text/css" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="{{ asset('/js/onlyie.js') }}"></script>
        <![endif]-->
    </head>
    <body>
        <div id="wrap">
            <header class="main-header">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#" style="padding-left:0px;padding-bottom: 0px; padding-top: 0px;">
                            <img src="{{asset('images/logo.jpg')}}" alt="Rejas DEACERO" />
                        </a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li ><a href="/">Reja Deacero</a></li>
                            <li class="headerDivider"></li>
                            <li><a href="{{ url('product') }}">Productos</a></li>
                            <li class="headerDivider"></li>
                            <li><a href="{{url('leed')}}">Leed y Sustentabilidad</a></li>
                            <li class="headerDivider"></li>
                            <li><a href="{{ url('warranty') }}">Garantia</a></li>
                            <li class="headerDivider"></li>
                            <li><a href="{{ url('quotation') }}">Cotizador</a></li>
                            <li class="headerDivider"></li>
                            <li><a href="{{ url('event') }}">Eventos</a></li>
                            <li class="headerDivider"></li>
                            <li><a href="{{url('almasa')}}">Almasa</a></li>
                            <li class="headerDivider"></li>
                            <li><a href="{{ url('contact')  }}">Contacto</a></li>

                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </nav>

        </header>

        <div class="footerBox">
            <div class="row" id="footerDivider">
                <div class="col-md-12"></div>
            </div>
            <div class="row" id="footerBar">
                <div class="col-xs-5 col-md-4" id="footerLogo">
                    <img src="{{ asset('images/almasalogo.jpg')  }}" alt="ALMASA"  />
                </div>
                <div class="col-xs-7 col-sm-7  col-md-7 " id="footerContacts">
                    <div class="row">
                        <div class="col-md-4">
                            <span>Almasa.</span> Todos Los derechos reservados&copy;
                        </div>
                        <div class="col-md-5">
                            <span>Tel&eacute;fonos:</span>
                            <div class="row">
                                <div class="col-xs-5 col-md-5">
                                    1.270 0777
                                </div>
                                <div class="col-xs-5 col-md-5">
                                    3212 138310
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <span>almasa.com.co</span><br /> rejadeacero@almasa.com.co
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('/js/vendor.js') }}"></script>
        <script type="application/javascript">
            @yield('extraJS')
        </script>
    </body>
</html>
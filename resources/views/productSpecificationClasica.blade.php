@extends('app')

@section('content')
    @include('productMenu')
    <div class="container-fluid prodDescBox">
        <div class="row">
            <div class="col-xs-12 col-md-9">
                <div class="row rowSpacer">
                    <div class="col-md-7 proDescHeader">
                        <span>ESPECIFICACIONES</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 proDescHeaderDivider"></div>
                </div>
                <div class="row" style="margin-left: 0px; margin-top: 20px;">
                    <div class="row">
                        <div class="col-xs-12 col-md-11">
                            <div class="row" style="margin-top: 0px; margin-bottom: 20px;">
                                <h4 class="tbl-header-specification">Panel</h4>
                                <div class="col-xs-12 col-md-11 proDescHeaderDivider"></div>
                            </div>
                            <div class="row">
                                <table class="col-xs-12 col-md-11 tbl-black-orange">
                            <thead>
                                <tr>
                                    <th>Altura</th>
                                    <th>Ancho</th>
                                    <th>Abertura</th>
                                    <th>Abrazaderas</th>
                                    <th colspan="2">Calibre</th>
                                    <th>Resistencia a la ruptura</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>m</td>
                                    <td>m</td>
                                    <td>mm</td>
                                    <td>#</td>
                                    <td>cal.</td>
                                    <td>mm</td>
                                    <td>Lb/plg2</td>
                                </tr>
                                <tr>
                                    <td>0.63</td>
                                    <td>2.50</td>
                                    <td>50 x 200</td>
                                    <td>2</td>
                                    <td>6</td>
                                    <td>4.89</td>
                                    <td>75,000 - 100,000</td>
                                </tr>
                                <tr>
                                    <td>1.00</td>
                                    <td>2.50</td>
                                    <td>50 x 200</td>
                                    <td>2</td>
                                    <td>6</td>
                                    <td>4.89</td>
                                    <td>75,000 - 100,000</td>
                                </tr>
                                <tr>
                                    <td>1.50</td>
                                    <td>2.50</td>
                                    <td>50 x 200</td>
                                    <td>3</td>
                                    <td>6</td>
                                    <td>4.89</td>
                                    <td>75,000 - 100,000</td>
                                </tr>
                                <tr>
                                    <td>1.82</td>
                                    <td>2.50</td>
                                    <td>50 x 200</td>
                                    <td>4</td>
                                    <td>6</td>
                                    <td>4.89</td>
                                    <td>75,000 - 100,000</td>
                                </tr>
                                <tr>
                                    <td>2.00</td>
                                    <td>2.50</td>
                                    <td>50 x 200</td>
                                    <td>4</td>
                                    <td>6</td>
                                    <td>4.89</td>
                                    <td>75,000 - 100,000</td>
                                </tr>
                                <tr>
                                    <td>2.40</td>
                                    <td>2.50</td>
                                    <td>50 x 200</td>
                                    <td>5</td>
                                    <td>6</td>
                                    <td>4.89</td>
                                    <td>75,000 - 100,000</td>
                                </tr>
                                <tr>
                                    <td>2.50</td>
                                    <td>2.22</td>
                                    <td>50 x 200</td>
                                    <td>5</td>
                                    <td>6</td>
                                    <td>4.89</td>
                                    <td>75,000 - 100,000</td>
                                </tr>
                            </tbody>
                        </table>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 40px;">
                        <div class="col-xs-12 col-md-11">
                            <div class="row" style="margin-top: 0px; margin-bottom: 20px;">
                                <h4 class="tbl-header-specification">Poste</h4>
                                <div class="col-xs-12 col-md-11 proDescHeaderDivider"></div>
                            </div>
                            <div class="row">
                                <table class="col-xs-12 col-md-11 tbl-black-orange">
                            <thead>
                                <tr>
                                    <th>Altura del panel</th>
                                    <th>Altura del poste</th>
                                    <th>Calibre</th>
                                    <th>Dimensiones</th>
                                    <th>Resistencia a la ruptura</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>m</td>
                                    <td>m</td>
                                    <td>cal.</td>
                                    <td>pig</td>
                                    <td>kgf</td>
                                </tr>
                                <tr>
                                    <td>0.63</td>
                                    <td>1.00</td>
                                    <td>16</td>
                                    <td>2 1/4” x 2 1/4”</td>
                                    <td>477</td>
                                </tr>
                                <tr>
                                    <td>1.00</td>
                                    <td>1.50</td>
                                    <td>16</td>
                                    <td>2 1/4” x 2 1/4”</td>
                                    <td>477</td>
                                </tr>
                                <tr>
                                    <td>1.50</td>
                                    <td>2.00</td>
                                    <td>16</td>
                                    <td>2 1/4” x 2 1/4”</td>
                                    <td>477</td>
                                </tr>
                                <tr>
                                    <td>2.00</td>
                                    <td>2.50</td>
                                    <td>16</td>
                                    <td>2 1/4” x 2 1/4”</td>
                                    <td>477</td>
                                </tr>
                                <tr>
                                    <td>2.50</td>
                                    <td>3.10</td>
                                    <td>16</td>
                                    <td>2 1/4” x 2 1/4”</td>
                                    <td>477</td>
                                </tr>
                            </tbody>
                        </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-3 rowSpacer">
                <img alt="" src="{{asset('images/products/clasicaspec1.jpg')}}" class="img-responsive" />
            </div>
        </div>
        <div class="row" style="margin-top: 60px;">
            <div class="col-xs-12 col-md-9">
                <div class="row rowSpacer">
                    <div class="col-md-7 proDescHeader">
                        <span>ACCESORIOS DE INSTALACI&Oacute;N</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 proDescHeaderDivider"></div>
                </div>
                <div class="row" style="margin-top: 100px;">
                    <ul class="clearfix ul-orange">
                        <li class="col-xs-12 col-md-2">
                            Tapa
                            <div>Polipropileno</div>
                            <img alt="" src="{{asset('images/products/tapaclasica.jpg')}}" class="img-responsive" />
                        </li>
                        <li class="col-xs-12 col-md-2">
                            Abrazadera
                            <div>Acero galvanizado</div>
                            <img alt="" src="{{asset('images/products/abrazadeera.jpg')}}" class="img-responsive" />
                        </li>
                        <li class="col-xs-12 col-md-2">
                            Bayoneta P&uacute;a
                            <div>Extensi&oacute;n a 45°</div>
                            <img alt="" src="{{asset('images/products/bayonetapua.jpg')}}" class="img-responsive" />
                        </li>
                        <li class="col-xs-12 col-md-2">
                            Bayoneta Panel
                            <div>Extensi&oacute;n a 45°</div>
                            <img alt="" src="{{asset('images/products/bayonetapanel.jpg')}}" class="img-responsive" />
                        </li>
                        <li class="col-xs-12 col-md-2">
                            Kit de base
                            <div>Acero galvanizado</div>
                            <img alt="" src="{{asset('images/products/kitbase.jpg')}}" class="img-responsive" />
                        </li>
                        <li class="col-xs-12 col-md-2">
                            Tuerca
                            <div class="text-bold-specifications">Autorrompiente</div>
                            {{--<img alt="" src="{{asset('images/products/')}}" class="img-responsive" />--}}
                        </li>
                    </ul>
                </div>
                <div class="row" style="margin-top: 80px; margin-bottom: 50px;">
                    <div class="col-xs-12 col-md-11 proDescHeaderDivider"></div>
                </div>
                <div class="row">
                    <ul class="ul-colors list-unstyled col-xs-12 col-md-12">
                        <li class="col-xs-12 col-md-1" style=" padding-top: 10px;">Colores</li>
                        <li class="col-xs-12 col-md-10">
                            <img alt="" src="{{asset('images/products/verde.jpg')}}" class="img-responsive" align="left" style="padding-left: 0px;" />
                            <img alt="" src="{{asset('images/products/negro.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/cafe.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/blanco.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/azul.jpg')}}" class="img-responsive" align="left"  style="padding-left: 10px;" />
                            <img alt="" src="{{asset('images/products/amarillo.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/rojo.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/gris.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                            <img alt="" src="{{asset('images/products/zinc.jpg')}}" class="img-responsive" align="left" style="padding-left: 10px;"  />
                        </li>
                    </ul>
                </div>
                <div class="row" style="margin-top: 50px; margin-bottom: 50px;">
                    <div class="col-xs-12 col-md-11 proDescHeaderDivider"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
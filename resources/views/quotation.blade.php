@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-12 textHeader">
            COTIZADOR
        </div>
    </div>
    <div class="container">
        <div class="row rowSpacer" style="margin-bottom: 40px;">
            <div class="col-md-12"  >
                <div class="row"   >
                    <div class="col-md-6  " >
                        <div  class="container-fluid prodDescBox"  >
                            <div class="row">
                                <div class="col-md-11 proDescHeader">
                                    <span>COTIZA EN L&Iacute;NEA</span>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-md-11  proDescHeaderDivider"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
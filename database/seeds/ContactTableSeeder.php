<?php
use App\Contact;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class ContactTableSeeder extends Seeder {

    public function run()
    {
        DB::table('contacts')->truncate();
        Contact::create(
            [
                'name'=>'CATALINA ECHEVERRI',
                'tel' => '3202 719 192',
                'email' => 'catalina.echeverri@almasa.com.co',
                'zone' => 'ZONA ANTIOQUIA'
            ]
        );
        Contact::create(
            [
                'name'=>'MARLON AGUDELO',
                'tel' => '3123 972 959',
                'email' => 'marlon.agudelo@almasa.com.co',
                'zone' => 'ZONA ATLÁNTICO'
            ]
        );
        Contact::create(
            [
                'name'=>'KARIN DELUQUE',
                'tel' => '3212 334 076',
                'email' => 'karin.deluque@almasa.com.co',
                'zone' => 'ZONA BOGOTÁ'
            ]
        );
        Contact::create(
            [
                'name'=>'KATHERINE TORRES',
                'tel' => '3212 138 310',
                'email' => 'katherine.torres@almasa.com.co',
                'zone' => 'DIRECTOR DE LÍNEA ARQUITECTURA'
            ]
        );
    }

}
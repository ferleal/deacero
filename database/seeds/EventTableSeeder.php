<?php
use App\Event;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class EventTableSeeder extends Seeder {

    public function run()
    {
        DB::table('events')->truncate();
        Event::create(
            [
                'name'=>'EXPOMETÁLICA',
                'location' => 'Medellín, Colombia',
                'eventDate' => '2013-04-22'
            ]
        );
        Event::create(
            [
                'name'=>'EXPOCAMACOL',
                'location' => 'Medellín, Colombia',
                'eventDate' => '2014-01-11'
            ]
        );
        Event::create(
            [
                'name'=>'ASOCRETO',
                'location' => 'Cartagena, Colombia',
                'eventDate' => '2014-02-14'
            ]
        );
        Event::create(
            [
                'name'=>'EXPOCONSTRUCCIÓN',
                'location' => 'Bogotá, Colombia',
                'eventDate' => '2015-03-15'
            ]
        );
        Event::create(
            [
                'name'=>'EXPOINFRAESTRUCTURA',
                'location' => 'Medellín, Colombia',
                'eventDate' => '2015-04-22'
            ]
        );
        Event::create(
            [
                'name'=>'CHARLA SOCIEDAD COLOMBIANA DE ARQUITECTOS SECCIONAL ATLÁNTICO',
                'location' => 'Barranquilla, Colombia.',
                'eventDate' => '2015-05-22'
            ]
        );
        Event::create(
            [
                'name'=>'EXPODEFENSA',
                'location' => '',
                'eventDate' => '2015-09-10'
            ]
        );
        Event::create(
            [
                'name'=>'CHARLA SOCIEDAD COLOMBIANA DE ARQUITECTOS SECCIONAL ANTIOQUIA',
                'location' => '',
                'eventDate' => '2015-10-22'
            ]
        );

    }

}
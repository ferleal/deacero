<?php

use App\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->truncate();
        Product::create([
            'name' => 'CLÁSICA',
            'routeName' => 'clasica',
            'image' => 'product_clasica.jpg',
            'description' => 'Es un sistema fácil de instalar que se caracteriza por tener un
diseño con pliegues en forma de “V” a lo largo y ancho de cada panel para dar mayor resistencia.',
            'features' => json_encode([['name' => 'Diseño Versátil', 'description' => 'La Reja Deacero Clásica tiene un diseño que puede utilizarse en diferentes tipos de proyectos.'], ['name' => 'Seguridad', 'description' => 'Su pliegue en forma de “V” le da estructura y firmeza al panel. Su diseño dificulta el que sea escalable, evitando la violación del perímetro.'], ['name' => 'Sistema de sujeción resistente', 'description' => 'Su sistema de sujeción está diseñado para otorgarle una alta resistencia estructural.']]),
            'utilization' => json_encode(['Parques de diversiones.', 'Áreas comerciales.', 'Hoteles.','Piscinas.', 'Áreas deportivas.', 'Áreas industriales.', 'Estacionamientos.', 'Áreas de juegos infantiles.', 'Zonas residenciales.', 'Plazas y parques.', 'Zoológicos.', 'Escuelas y universidades.', 'Otras.']),
            'icon' => json_encode(['icon_milan_2.png', 'icon_milan_1.png']),
            'video' => 'https://www.youtube.com/embed/YgmIibSnZs0',
            'specifications_file' => 'demo.pdf',
            'description_file' => 'demo.pdf',
            'quality_file' => 'demo.pdf',
            'brochure_file' => 'demo.pdf',
            'manual_file' => 'demo.pdf',
            'maintenance_file' => 'demo.pdf',
        ]);
        Product::create([
            'name' => 'MILÁN',
            'routeName' => 'milan',
            'image' => 'product_milan.jpg',
            'description' => 'Está fabricada con alambre continuo que le permite tener acabados curvos en su parte superior e inferior y secciones rectangulares en la parte central.',
            'features' => json_encode([['name' => 'Estética', 'description' => 'La Reja Deacero Milán está fabricada con un diseño arquitectónico que embellece su entorno.'], ['name' => 'Acabados seguros', 'description' => 'Los acabados curvos y suaves del panel aseguran que no dañen a quienes se encuentran dentro o fuera del perímetro.'], ['name' => 'Única en el mercado', 'description' => 'Es la única reja con sus características, gracias a su diseño patentado.']]),
            'utilization' => json_encode(['Parques de diversiones.', 'Áreas comerciales.', 'Oficinas corporativas.', 'Hoteles.', 'Áreas de juegos infantiles.', 'Zonas residenciales.', 'Viñedos.', 'Estacionamientos.', 'Zoológicos.', 'Escuelas y universidades.', 'Otras.']),
            'icon' => json_encode(['icon_milan_2.png', 'icon_milan_1.png']),
            'video' => 'https://www.youtube.com/embed/YgmIibSnZs0',
            'specifications_file' => 'demo.pdf',
            'description_file' => 'demo.pdf',
            'quality_file' => 'demo.pdf',
            'brochure_file' => 'demo.pdf',
            'manual_file' => 'demo.pdf',
            'maintenance_file' => 'demo.pdf',
        ]);
        Product::create([
            'name' => 'FLORENCIA',
            'routeName' => 'florencia',
            'image' => 'product_florencia.jpg',
            'description' => 'Se caracteriza por su fabricación con alambre continuo formando un diseño único de arcos y curvas en todo el panel.',
            'features' => json_encode([['name' => 'Protección de áreas', 'description' => 'Gracias a su baja altura, es la opción perfecta para delimitar áreas de juego, parques, jardineras, piscinas y andadores.'], ['name' => 'Acabados seguros', 'description' => 'Los acabados curvos y suaves del panel aseguran que no dañen a quienes se encuentran dentro o fuera del perímetro.'], ['name' => 'Única en el mercado', 'description' => 'Es la única reja con sus características, gracias a su diseño patentado.']]),
            'utilization' => json_encode(['Parques de diversiones.', 'Áreas comerciales.', 'Oficinas corporativas.', 'Hoteles.', 'Áreas de juegos infantiles.', 'Zonas residenciales.', 'Viñedos.', 'Estacionamientos.', 'Zoológicos.', 'Escuelas y universidades.', 'Otras.']),
            'icon' => json_encode(['icon_milan_2.png', 'icon_milan_1.png']),
            'video' => 'https://www.youtube.com/embed/YgmIibSnZs0',
            'specifications_file' => 'demo.pdf',
            'description_file' => 'demo.pdf',
            'quality_file' => 'demo.pdf',
            'brochure_file' => 'demo.pdf',
            'manual_file' => 'demo.pdf',
            'maintenance_file' => 'demo.pdf',
        ]);
        Product::create([
            'name' => 'CONTEMPORÁNEA',
            'routeName' => 'contemporanea',
            'image' => 'product_conteporanea.jpg',
            'description' => 'Es un sistema integral de alambres soldados con diseño minimalista. Tiene gran resistencia gracias a su doble alambre de refuerzo horizontal.',
            'features' => json_encode([['name' => 'Diseño minimalista y uniforme', 'description' => 'La Reja Deacero Contemporánea presenta un diseño de acabados planos y uniformes donde la sobriedad es la prioridad.'], ['name' => 'Seguridad', 'description' => 'Los dos alambres horizontales de refuerzo, uno a cada lado del panel, le dan resistencia a impactos y recargones'], ['name' => 'Sistema de sujeción resistente', 'description' => 'Su sistema de sujeción está diseñado para otorgarle una alta resistencia estructural.']]),
            'utilization' => json_encode(['Parques de diversiones.', 'Áreas comerciales.', 'Oficinas corporativas.', 'Hoteles.', 'Instalaciones de gobierno.', 'Áreas industriales.', 'Parques.', 'Estacionamientos.', 'Piscinas.', 'Escuelas y universidades.', 'Áreas deportivas.', 'Otras.']),
            'icon' => json_encode(['icon_milan_2.png', 'icon_milan_1.png']),
            'video' => 'https://www.youtube.com/embed/YgmIibSnZs0',
            'specifications_file' => 'demo.pdf',
            'description_file' => 'demo.pdf',
            'quality_file' => 'demo.pdf',
            'brochure_file' => 'demo.pdf',
            'manual_file' => 'demo.pdf',
            'maintenance_file' => 'demo.pdf',
        ]);
        Product::create([
            'name' => 'FORTE',
            'routeName' => 'forte',
            'image' => 'product_forte.jpg',
            'description' => 'Su diseño de aberturas prácticamente impenetrables es imposible de escalar, esto la convierte en la mejor solución para áreas de alta seguridad.',
            'features' => json_encode([['name' => 'Difícil de escalar', 'description' => 'Las aberturas de la Reja Deacero Forte son sumamente cerradas, lo que la convierte en un sistema antiescalable.'], ['name' => 'Resistencia estructural', 'description' => 'Componentes diseñados para una alta resistencia.'], ['name' => 'Versátil', 'description' => 'Dos diseños disponibles: panel plano o con pliegues.'], ['name' => 'Impenetrable', 'description' => 'Su diseño dificulta el poder cortar sus alambres con pinzas o cizallas.']]),
            'utilization' => json_encode(['Aeropuertos.', 'Instalaciones de gobierno.', 'Ferrocarriles.', 'Zonas militares.', 'Estaciones eléctricas.', 'Zoológicos.', 'Prisiones.', 'Otras.']),
            'icon' => json_encode(['icon_milan_2.png', 'icon_milan_1.png']),
            'video' => 'https://www.youtube.com/embed/YgmIibSnZs0',
            'specifications_file' => 'demo.pdf',
            'description_file' => 'demo.pdf',
            'quality_file' => 'demo.pdf',
            'brochure_file' => 'demo.pdf',
            'manual_file' => 'demo.pdf',
            'maintenance_file' => 'demo.pdf',
        ]);
    }
}

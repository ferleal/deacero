<?php

use App\ProductsGallery;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsGalleryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products_galleries')->truncate();
        $i = 0;
        $productId = 1;
        $productName = 'clasica';
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal1.jpg',
            'description' => 'Parque Salvador Allende, Lima, Perú.',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal2.jpg',
            'description' => 'Parque de Perros, Baltimore, E.U.A.',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal3.jpg',
            'description' => 'Administración Portuaria, Puerto Vallarta, Mx.',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal4.jpg',
            'description' => 'Av. Kennedy, Santiago, Chile.',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal5.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal6.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal7.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal8.jpg',
            'description' => 'Autodromo Miguel E. Abed, Puebla, México.',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal9.jpg',
            'description' => 'Union Pacific Railway, Arizona, E.U.A.',
            'order' => ++$i,
        ]);
        $i = 0;
        $productId = 2;
        $productName = 'milan';
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal1.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal2.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal3.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal4.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal5.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal6.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal7.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal8.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal9.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        $i = 0;
        $productId = 3;
        $productName = 'florencia';
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal1.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal2.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal3.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal4.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal5.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal6.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal7.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal8.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal9.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        $i = 0;
        $productId = 4;
        $productName = 'contemporanea';
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal1.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal2.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal3.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal4.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal5.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal6.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal7.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal8.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal9.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        $i = 0;
        $productId = 5;
        $productName = 'forte';
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal1.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal2.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal3.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal4.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal5.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal6.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal7.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal8.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
        ProductsGallery::create([
            'product_id' => $productId,
            'picture' => $productName.'gal9.jpg',
            'description' => '',
            'order' => ++$i,
        ]);
    }
}

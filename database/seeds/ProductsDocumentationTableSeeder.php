<?php

use App\ProductsDocumentation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsDocumentationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products_documentations')->truncate();
        $productId = 1;
        $order = 0;
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '0.63',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '1.00',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '1.50',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '2.00',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '2.50',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        $productId = 2;
        $order = 0;
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '0.63',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '1.00',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '1.50',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '2.00',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '2.50',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        $productId = 3;
        $order = 0;
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '0.63',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '1.00',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '1.50',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '2.00',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '2.50',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        $productId = 4;
        $order = 0;
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '0.63',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '1.00',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '1.50',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '2.00',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '2.50',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        $productId = 5;
        $order = 0;
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '0.63',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '1.00',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '1.50',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '2.00',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
        ProductsDocumentation::create([
            'product_id' => $productId,
            'panel' => '2.50',
            'file_dwg' => '',
            'file_pdf' => 'demo.pdf',
            'order' => ++$order,
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);
        $this->call(ContactTableSeeder::class);
        $this->call(EventTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(ProductsGalleryTableSeeder::class);
        $this->call(ProductsDocumentationTableSeeder::class);

        Model::reguard();
    }
}

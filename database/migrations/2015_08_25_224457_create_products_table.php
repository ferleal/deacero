<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('routeName');
            $table->string('image');
            $table->string('description');
            $table->text('features');
            $table->text('utilization');
            $table->text('icon');
            $table->text('video');
            $table->text('specifications_file');
            $table->text('description_file');
            $table->text('quality_file');
            $table->text('brochure_file');
            $table->text('manual_file');
            $table->text('maintenance_file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
